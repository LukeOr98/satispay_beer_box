package com.satispay.feature.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.satispay.feature.R
import com.satispay.feature.model.Beer
import com.satispay.feature.util.ImageManager
import com.satispay.feature.util.runOnUiThread
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

class BeersAdapter(private val moreInfoPressed: (Beer) -> Unit) : RecyclerView.Adapter<BeersAdapter.ViewHolder>() {

    private var beers: MutableList<Beer> = mutableListOf()

    fun addBeers(newBeers: List<Beer>) {
        val oldSize = beers.size
        beers.addAll(newBeers)
        notifyItemRangeInserted(oldSize, oldSize + newBeers.size)
    }

    fun clearBeers() {
        val oldSize = beers.size
        beers.clear()
        notifyItemRangeRemoved(0, oldSize)
    }

    inner class ViewHolder(itemView: View, private val moreInfoPressed: (Beer) -> Unit) : RecyclerView.ViewHolder(itemView) {
        private val nameTextView: TextView = itemView.findViewById(R.id.tv_beer_name)
        private val taglineTextView: TextView = itemView.findViewById(R.id.tv_beer_tagline)
        private val descriptionTextView: TextView = itemView.findViewById(R.id.tv_beer_description)
        private val bottleImageView: ImageView = itemView.findViewById(R.id.iv_bottle)
        private val moreInfoButton: Button = itemView.findViewById(R.id.btn_more_info)

        fun bind(beer: Beer) {
            nameTextView.text = beer.name
            taglineTextView.text = beer.tagline
            descriptionTextView.text = beer.description
            moreInfoButton.setOnClickListener {
                moreInfoPressed(beer)
            }
            CoroutineScope(IO).launch {
                ImageManager.downloadImage(beer.imageUrl ?: "")?.let {
                    bottleImageView.context.runOnUiThread {
                        bottleImageView.setImageBitmap(it)
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            itemView = LayoutInflater.from(parent.context).inflate(R.layout.beer_item, parent, false),
            moreInfoPressed = moreInfoPressed
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(beers[position])
    }

    override fun getItemCount(): Int =
        beers.size

}