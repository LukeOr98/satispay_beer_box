package com.satispay.feature.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.satispay.feature.model.Beer
import com.satispay.feature.networking.BeersManager
import kotlinx.coroutines.launch

class BeersViewModel : ViewModel() {
    val beers = MutableLiveData<List<Beer>?>()

    private var lastDownloadedPage = 0

    var textQuery: String = ""

    fun refreshBeers() {
        if(textQuery == "") {
            getBeers()
        } else {
            searchBeersByName(textQuery)
        }
    }

    fun clear() {
        lastDownloadedPage = 0
        beers.postValue(null)
    }

    private fun getBeers() = viewModelScope.launch {
        beers.postValue(BeersManager.getBeers(++lastDownloadedPage))
    }

    private fun searchBeersByName(name: String) = viewModelScope.launch {
        beers.postValue(BeersManager.searchBeersByName(++lastDownloadedPage, name))
    }
}