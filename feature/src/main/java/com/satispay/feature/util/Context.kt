package com.satispay.feature.util

import android.content.Context
import android.os.Handler

fun Context.runOnUiThread(unit: () -> Unit) {
    Handler(this.mainLooper).post(unit)
}