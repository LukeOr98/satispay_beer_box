package com.satispay.feature.util

import android.util.Log
import androidx.annotation.IntDef
import com.satispay.feature.BuildConfig
import okhttp3.logging.HttpLoggingInterceptor

object LogHelper : HttpLoggingInterceptor.Logger {
    private val DEBUG: Boolean = BuildConfig.DEBUG
    var prefix: String = ""

    @Suppress("MemberVisibilityCanBePrivate")
    fun log(
        tag: String,
        message: String,
        @LogLevels level: Int = Log.DEBUG,
        forceLogInRelease: Boolean = false
    ) {
        if (DEBUG || forceLogInRelease) {
            when (level) {
                Log.ASSERT -> {
                    Log.wtf("${prefix}__$tag", message)
                }
                Log.ERROR -> {
                    Log.e("${prefix}__$tag", message)
                }
                Log.WARN -> {
                    Log.w("${prefix}__$tag", message)
                }
                Log.DEBUG -> {
                    Log.d("${prefix}__$tag", message)
                }
                Log.INFO -> {
                    Log.i("${prefix}__$tag", message)
                }
                Log.VERBOSE -> {
                    Log.v("${prefix}__$tag", message)
                }
            }
        }
    }

    override fun log(message: String) {
        log("Retrofit", message, Log.VERBOSE)
    }
}

@IntDef(
    Log.ASSERT,
    Log.ERROR,
    Log.WARN,
    Log.DEBUG,
    Log.INFO,
    Log.VERBOSE
)
@Retention(AnnotationRetention.SOURCE)
internal annotation class LogLevels