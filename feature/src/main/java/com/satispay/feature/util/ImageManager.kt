package com.satispay.feature.util

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import java.net.URL
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


object ImageManager {
    @Suppress("BlockingMethodInNonBlockingContext")
    suspend fun downloadImage(url: String): Bitmap? =
        suspendCoroutine { cont ->
            cont.resume(
                try {
                    BitmapFactory.decodeStream(URL(url).openConnection().getInputStream())
                } catch(e: Exception) {
                    null
                }
            )
        }
}