package com.satispay.feature.networking

import com.satispay.feature.model.Beer
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface BeerBoxService {
    @GET("beers")
    fun getBeers(@Query("page") page: Int): Call<List<Beer>>
    @GET("beers")
    fun searchBeersByName(@Query("page") page: Int, @Query("beer_name") name: String): Call<List<Beer>>
}