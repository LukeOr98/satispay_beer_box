package com.satispay.feature.networking

import com.satispay.feature.util.LogHelper
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object BeersProvider {
    val beersService: BeerBoxService = Retrofit.Builder()
        .baseUrl("https://api.punkapi.com/v2/")
        .client(
            OkHttpClient
                .Builder()
                .apply {
                    addInterceptor(
                        HttpLoggingInterceptor(LogHelper).apply {
                            level = HttpLoggingInterceptor.Level.BODY
                        }
                    )
                }.build()
        )
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(BeerBoxService::class.java)
}