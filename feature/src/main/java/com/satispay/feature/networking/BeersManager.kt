package com.satispay.feature.networking

import com.satispay.feature.model.Beer
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

object BeersManager {
    suspend fun getBeers(page: Int) : List<Beer> =
        suspendCoroutine { cont ->
            BeersProvider.beersService.getBeers(page)
                .enqueue(
                    object : Callback<List<Beer>> {
                        override fun onResponse(
                            call: Call<List<Beer>>,
                            response: Response<List<Beer>>
                        ) {
                            if(response.isSuccessful) {
                                cont.resume(response.body() ?: listOf())
                            }
                            else {
                                cont.resume(listOf())
                            }
                        }

                        override fun onFailure(call: Call<List<Beer>>, t: Throwable) {
                            cont.resume(listOf())
                        }

                    }
                )
        }


    suspend fun searchBeersByName(page: Int, name: String) : List<Beer> =
        suspendCoroutine { cont ->
            BeersProvider.beersService.searchBeersByName(page, name.replace(" ", "_"))
                .enqueue(
                    object : Callback<List<Beer>> {
                        override fun onResponse(
                            call: Call<List<Beer>>,
                            response: Response<List<Beer>>
                        ) {
                            if(response.isSuccessful) {
                                cont.resume(response.body() ?: listOf())
                            }
                            else {
                                cont.resume(listOf())
                            }
                        }

                        override fun onFailure(call: Call<List<Beer>>, t: Throwable) {
                            cont.resume(listOf())
                        }

                    }
                )
        }
}