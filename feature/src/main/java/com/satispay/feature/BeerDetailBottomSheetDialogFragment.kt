package com.satispay.feature

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.satispay.feature.databinding.BeerDetailBinding
import com.satispay.feature.model.Beer
import com.satispay.feature.util.ImageManager
import com.satispay.feature.util.runOnUiThread
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class BeerDetailBottomSheetDialogFragment(private val beer: Beer) : BottomSheetDialogFragment() {

    private val binding by lazy {
        BeerDetailBinding.inflate(layoutInflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
    }

    private fun setupView() {
        binding.apply {
            tvBeerNameDetail.text = beer.name
            tvBeerTaglineDetail.text = beer.tagline
            tvBeerDescriptionDetail.text = beer.description
            CoroutineScope(Dispatchers.IO).launch {
                ImageManager.downloadImage(beer.imageUrl ?: "")?.let {
                    requireContext().runOnUiThread {
                        ivBottleDetail.setImageBitmap(it)
                    }
                }
            }
        }
    }
}