package com.satispay.feature

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.satispay.feature.adapter.BeersAdapter
import com.satispay.feature.databinding.ActivityBeersBinding
import com.satispay.feature.util.LogHelper
import com.satispay.feature.viewmodel.BeersViewModel

/**
 * Using a simple MVVM as architectural pattern,
 * since it makes possible the ability of automatically
 * updating the view with the newly downloaded list of beers.
 * It would make it simpler to implement filters too.
 */
class BeersActivity : AppCompatActivity() {

    private val binding by lazy {
        ActivityBeersBinding.inflate(layoutInflater)
    }

    private val viewModel by viewModels<BeersViewModel>()

    private val adapter by lazy {
        BeersAdapter { beer ->
            BeerDetailBottomSheetDialogFragment(beer).show(supportFragmentManager, "beer_detail")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        LogHelper.prefix = "BeerBox"
        setupView()
        setupObservers()
        viewModel.refreshBeers()
    }

    private fun setupView() {
        binding.apply {
            rvBeers.adapter = adapter
            rvBeers.layoutManager = LinearLayoutManager(baseContext)
            rvBeers.addOnScrollListener(
                object : RecyclerView.OnScrollListener() {
                    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                        if (!recyclerView.canScrollVertically(1)) {
                            viewModel.refreshBeers()
                        }
                    }
                }
            )
            etSearch.doAfterTextChanged { text ->
                viewModel.clear()
                viewModel.textQuery = text.toString()
                viewModel.refreshBeers()
            }
        }
    }

    private fun setupObservers() {
        viewModel.beers.observe(this) {
            if (it == null) {
                adapter.clearBeers()
            } else {
                adapter.addBeers(it)
            }
        }
    }
}