package com.satispay.beerbox

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.satispay.feature.BeersActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()
        // Don't know what should happen during the splash screen,
        // so I put a timer here just so it doesn't disappear immediately
        Handler(Looper.getMainLooper())
            .postDelayed(
                {
                    startActivity(
                        Intent(
                            this,
                            BeersActivity::class.java
                        ).apply {
                            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        }
                    )
                },
                3000
            )
    }
}